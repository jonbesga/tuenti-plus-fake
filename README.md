# Tuenti Plus Fake (2009 project)
If I remember correctly, a logic-bomb project to close browsers to the user every few minutes.
I think this was my first 'real' programming project. As you can see here: http://www.tuentiadictos.es/tuenti-plus-fake/. It had
a not very believable interface.
