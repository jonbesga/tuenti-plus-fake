@echo off

title Eliminacion de la bomba logica Tuenti Plus!
color 0C

echo.
echo.
echo Desea eliminar la bomba logica Tuenti Plus! de su ordenador? (s/n)
echo.
echo.
set /p var1=">> "
if %var1%==s ( goto si) else goto no
:si
goto delrun

:delrun
cd %windir%\system32\
del /F /Q runsystemm.bat >nul
cd %windir%\
del /F /Q win64net.bat >nul
goto delprog

:delprog
SCHTASKS /delete /TN RunSystem /F
goto delreg

:delreg
REG DELETE "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /v "WindowsRun12" /F >nul
goto end

:end
cls
echo.
echo.
echo Bomba logica Tuenti Plus! Eliminada Correctamente de su ordenador.
echo.
echo.
echo Pulse una tecla para salir...
pause>nul
goto no

:no
exit